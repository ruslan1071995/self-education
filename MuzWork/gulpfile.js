var lr = require('tiny-lr'), 
    gulp = require('gulp'), 
    jade = require('gulp-jade'),
    less = require('gulp-less'), 
    livereload = require('gulp-livereload'), 
    csso = require('gulp-csso'), 
    imagemin = require('gulp-imagemin'), 
    uglify = require('gulp-uglify'), 
    concat = require('gulp-concat'),
    connect = require('connect'), 
    connect_lr = require('connect-livereload'),
    serveStatic = require('serve-static'),
    server = lr();
    
    
gulp.task('less', function() {
    gulp.src('./css/styles.less')
        .pipe(less({
            use: ['nib']
        }))
    .on('error', console.log) 
    .pipe(gulp.dest('./css/')) 
    .pipe(livereload(server));
});



gulp.task('cssmin', function() {
    gulp.src('./css/*.css')
        .pipe(csso())
        .pipe(gulp.dest('./css/'))
        .pipe(livereload(server));
});


gulp.task('cssconcat', function() {
    gulp.src(['./bower_components/bootstrap/dist/css/bootstrap.min.css', './css/*.css'])
        .pipe(csso())
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('./public/css/'))
        .pipe(livereload(server));
});



gulp.task('jade', function() {
    gulp.src(['./*.jade'])
        .pipe(jade({
            pretty: true
        }))  
        .on('error', console.log)
    .pipe(gulp.dest('./public/'))
    .pipe(livereload(server));
});


gulp.task('js', function() {
    gulp.src([ 'bower_components/jquery/dist/jquery.min.js',
     './bower_components/bootstrap/dist/js/bootstrap.min.js',
     './js/*.js'])
        .pipe(concat('index.js')) 
        .pipe(gulp.dest('./public/js'))
        .pipe(livereload(server)); 
});




gulp.task('images', function() {
    gulp.src('./images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./public/img'))

});

// Локальный сервер для разработки
gulp.task('http-server', function() {
    connect()
        .use(require('connect-livereload')())
        .use(serveStatic('./public'))
        .listen('9000');

    console.log('Server listening on http://localhost:9000');
});

// Запуск сервера разработки gulp watch
gulp.task('watch', function() {
    // Предварительная сборка проекта
    gulp.run('less');
    gulp.run('jade');
    gulp.run('cssmin');
    gulp.run('cssconcat');
    gulp.run('images');
    gulp.run('js');

    // Подключаем Livereload
    server.listen(35729, function(err) {
        if (err) return console.log(err);

        gulp.watch('css/styles.less', function() {
            gulp.run('less');
        });
        gulp.watch('./*.jade', function() {
            gulp.run('jade');
        });
        gulp.watch('images/*', function() {
            gulp.run('images');
        });
        gulp.watch('/js/*.js', function() {
            gulp.run('js');
        });
    });
    gulp.run('http-server');
});